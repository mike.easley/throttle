interface ThrottleOptions {
  delay: number;
  leading?: boolean;
  onError?: (error: Error | unknown) => void;
}

type Handler = (...args: any[]) => any;

export function throttle<H extends Handler, O extends ThrottleOptions>(
  handler: H,
  options: O
) {
  let cancelToken: number | ReturnType<typeof setTimeout> = 0;
  let lastReturnValue: ReturnType<H>;
  let lastArgs: Parameters<H>;

  function throttled(this: ThisParameterType<H>, ...args: Parameters<H>) {
    if (options.leading) {
      if (cancelToken) {
        return lastReturnValue;
      }

      throttled.invoke(this, args);
    }

    if (!cancelToken) {
      cancelToken = setTimeout(() => {
        cancelToken = 0;
        if (options.leading) {
          return;
        }
        throttled.invoke(this, lastArgs || args);
      }, options.delay);
    } else {
      lastArgs = args;
    }

    return lastReturnValue;
  }

  function invoke(context: ThisParameterType<H>, handlerArgs: Parameters<H>) {
    try {
      lastReturnValue = handler.apply(context, handlerArgs);
      return lastReturnValue;
    } catch (error) {
      if (options.onError) {
        options.onError(error);
      } else {
        throw error;
      }
    }
  }

  function cancel(): void {
    if (!cancelToken) {
      return;
    }

    clearTimeout(cancelToken);
    cancelToken = 0;
  }

  throttled.invoke = invoke;
  throttled.cancel = cancel;

  return throttled;
}
